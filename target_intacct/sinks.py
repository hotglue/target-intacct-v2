"""intacct target sink class, which handles writing streams."""

from __future__ import annotations

from datetime import datetime
from typing import Any, Dict, List, Mapping, Optional, Union

from singer_sdk.plugin_base import PluginBase
from singer_sdk.sinks import RecordSink

from target_intacct.mapping import UnifiedMapping

from .client import SageIntacctSDK, get_client
from .const import DEFAULT_API_URL, KEY_PROPERTIES, REQUIRED_CONFIG_KEYS
import re
# import xmltodict


class intacctSink(RecordSink):
    """intacct target sink class."""

    def __init__(
        self,
        target: PluginBase,
        stream_name: str,
        schema: Dict,
        key_properties: Optional[List[str]],
    ) -> None:
        super().__init__(target, stream_name, schema, key_properties)

        self.target_name = "intacct-v2"

        self.client = get_client(
            api_url=target.config.get("api_url", DEFAULT_API_URL),
            company_id=target.config["company_id"],
            sender_id=target.config["sender_id"],
            sender_password=target.config["sender_password"],
            user_id=target.config["user_id"],
            user_password=target.config["user_password"],
            headers={"User-Agent": target.config["user_agent"]}
            if "user_agent" in target.config
            else {},
            use_locations=target.config.get("use_locations", False) and self.stream_name != "Suppliers",
            location_id=target.config.get("location_id")
        )

        self.vendors = None
        self.locations = None
        self.accounts = None
        self.items = None
        self.banks = None
        self.classes = None
        self.projects = None
        self.departments = None
        self.customers = None
        self.journal_entries = None

    def get_vendors(self):
        # Lookup for vendors
        if self.vendors is None:
            vendors = self.client.get_entity(
                object_type="accounts_payable_vendors", fields=["VENDORID", "NAME"]
            )
            self.vendors = self.dictify(vendors, "NAME", "VENDORID")
        return self.vendors

    def get_classes(self):
        # Lookup for vendors
        if self.classes is None:
            classes = self.client.get_entity(
                object_type="classes", fields=["CLASSID", "NAME"]
            )
            self.classes = self.dictify(classes, "NAME", "CLASSID")
        return self.classes

    def get_projects(self):
        # Lookup for vendors
        if self.projects is None:
            projects = self.client.get_entity(
                object_type="projects", fields=["PROJECTID", "NAME"]
            )
            self.projects = self.dictify(projects, "NAME", "PROJECTID")
        return self.projects

    def get_locations(self):
        # Lookup for Locations
        if self.locations is None:
            locations = self.client.get_entity(
                object_type="locations", fields=["LOCATIONID", "NAME"]
            )
            self.locations = self.dictify(locations, "NAME", "LOCATIONID")
        return self.locations

    def get_accounts(self):
        if self.accounts is None:
            # Lookup for accounts
            accounts = self.client.get_entity(
                object_type="general_ledger_accounts",
                fields=["RECORDNO", "ACCOUNTNO", "TITLE"],
            )
            self.accounts = self.dictify(accounts, "TITLE", "ACCOUNTNO")
            self.accounts_recordno = self.dictify(accounts, "RECORDNO", "ACCOUNTNO")
        return self.accounts

    def get_departments(self):
        if self.departments is None:
            # Lookup for accounts
            departments = self.client.get_entity(
                object_type="departments",
                fields=["DEPARTMENTID", "TITLE"],
            )
            self.departments = self.dictify(departments, "TITLE", "DEPARTMENTID")
        return self.departments

    def get_items(self):
        if self.items is None:
            # Lookup for items
            items = self.client.get_entity(
                object_type="item", fields=["ITEMID", "NAME"]
            )
            self.items = self.dictify(items, "NAME", "ITEMID")
        return self.items

    def get_customers(self):
        # Lookup for customers
        if self.customers is None:
            customers = self.client.get_entity(
                object_type="customers", fields=["CUSTOMERID", "NAME"]
            )
            self.customers = self.dictify(customers, "NAME", "CUSTOMERID")
        return self.customers

    def get_journal_entries(self):
        # Lookup for journal_entries
        if self.journal_entries is None:
            journal_entries = self.client.get_entity(
                object_type="general_ledger_journal_entries", fields=["BATCH_TITLE", "RECORDNO"]
            )
            self.journal_entries = self.dictify(journal_entries, "BATCH_TITLE", "RECORDNO")
        return self.journal_entries


    def dictify(sefl, array, key, value):
        array_ = {}
        for i in array:
            array_[i[key]] = i[value]
        return array_

    def post_attachments(self, payload, record):
        mapping = UnifiedMapping(config=self.config)
        #prepare attachment payload
        att_payload = mapping.prepare_attachment_payload(record)
        if att_payload:
            att_id = att_payload["create_supdoc"]["supdocid"]
            #1. create folder
            #check if the folder exists:
            check_folder = {"get":{"@object": "supdocfolder", "@key": att_id}}
            folder = self.client.format_and_send_request(check_folder)

            if folder.get("data", {}).get("supdocfolder"):
                self.logger.info(f"Folder with name {att_id} already exists")
            else:
                # if folder doesn't exist create folder
                folder_payload = {"create_supdocfolder": {"supdocfoldername": att_id, "object": "supdocfolder"}}
                self.client.format_and_send_request(folder_payload)
            #2. post attachments
            #check if supdoc exists
            check_supdoc = {"get":{"@object": "supdoc", "@key": att_id}}
            supdoc = self.client.format_and_send_request(check_supdoc) or dict()
            supdoc = supdoc.get("data", {}) or dict()

            #updating existing supdoc
            supdoc = supdoc.get("supdoc")
            if supdoc:
                self.logger.info(f"supdoc with id {att_id} already exists, updating existing supdoc")
                attachments = supdoc.get("attachments", {}).get("attachment")
                #getting a list of existing attachments to avoid duplicates
                existing_attachments = {"content":[], "names":[]}
                if isinstance(attachments, dict):
                    existing_attachments["names"] = [attachments.get("attachmentname")]
                    existing_attachments["content"] = (attachments.get("attachmentdata"))
                elif isinstance(attachments, list):
                    existing_attachments["content"] = [att.get("attachmentdata") for att in attachments]
                    existing_attachments["names"] = [att.get("attachmentname") for att in attachments]
                #update att_payload to
                att_payload = mapping.prepare_attachment_payload(record, "update", existing_attachments)
            #send attachments
            if att_payload:
                self.client.format_and_send_request(att_payload)
            return att_id
        return None

    def purchase_invoices_upload(self, record):

        # Format data
        mapping = UnifiedMapping()
        payload = mapping.prepare_payload(record, "purchase_invoices", self.target_name)

        # Check if the invoice exists
        bill = None
        if payload.get("RECORDID"):
            # validate RECORDID
            invalid_chars = r"[\"\'&<>#?]"  # characters not allowed for RECORDID [&, <, >, #, ?]
            is_id_valid = not bool(re.search(invalid_chars, payload.get("RECORDID")))

            if not is_id_valid:
                raise Exception(
                    f"RECORDID '{payload.get('RECORDID')}' contains one or more invalid characters '&,<,>,#,?'. Please provide a RECORDID that does not include these characters."
                )
            # check if record exists
            bill = self.client.get_entity(object_type="accounts_payable_bills", fields=["RECORDNO", "STATE", "VENDORNAME", "BASECURR"], filter={"filter": {"equalto":{"field":"RECORDID","value": payload.get("RECORDID")}}})

        #send attachments
        supdoc_id = self.post_attachments(payload, record)
        if supdoc_id:
            payload["SUPDOCID"] = supdoc_id
        
        # Map action
        action = payload.get("STATE")
        if action:
            if action.lower() == "draft":
                payload["ACTION"] = "Draft"
        payload.pop("STATE", None)

        # Get the matching values for the payload :
        # Matching "VENDORNAME" -> "VENDORID"
        self.get_vendors()
        if payload.get("VENDORNAME") and not payload.get("VENDORID"):
            vendor_name = self.vendors.get(payload["VENDORNAME"])
            if vendor_name:
                payload["VENDORID"] = self.vendors[payload["VENDORNAME"]]
            else:
                raise Exception(
                    f"ERROR: VENDORNAME {payload['VENDORNAME']} not found for this account."
                )
        payload.pop("VENDORNAME", None)
        
        if payload.get("VENDORID") not in self.vendors.values():
            raise Exception(
                f"ERROR: VENDORID {payload['VENDORID']} not found for this account."
            )

        # Matching ""
        for item in payload.get("APBILLITEMS").get("APBILLITEM"):
            if item.get("LOCATIONNAME"):
                self.get_locations()
                location = self.locations.get(item["LOCATIONNAME"])
                if location:
                    item["LOCATIONID"] = self.locations.get(item["LOCATIONNAME"])
                else:
                    raise Exception(f"Location '{payload['LOCATIONNAME']}' does not exist. Did you mean any of these: {list(self.locations.keys())}?")
            elif payload.get("LOCATIONNAME"):
                self.get_locations()
                location = self.locations.get(payload["LOCATIONNAME"])
                if location:
                    item["LOCATIONID"] = self.locations.get(payload["LOCATIONNAME"])
                else:
                    raise Exception(f"Location '{payload['LOCATIONNAME']}' does not exist. Did you mean any of these: {list(self.locations.keys())}?")

            if item.get("VENDORNAME") and not item.get("VENDORID"):
                self.get_vendors()
                item["VENDORID"] = self.vendors.get(item["VENDORNAME"])
            item.pop("VENDORNAME", None)

            if item.get("CLASSNAME"):
                self.get_classes()
                item["CLASSID"] = self.classes[item["CLASSNAME"]]
                item.pop("CLASSNAME")

            if item.get("PROJECTNAME"):
                self.get_projects()
                project_id = self.projects.get(item["PROJECTNAME"])
                if project_id:
                    item["PROJECTID"] = project_id
                item.pop("PROJECTNAME")

            #add custom fields to the item payload
            custom_fields = item.pop("customFields", None)
            if custom_fields:
                [item.update({cf.get("name"): cf.get("value")}) for cf in custom_fields]

            self.get_accounts()
            if item.get("ACCOUNTID"):
                item["ACCOUNTNO"] = next(( self.accounts_recordno.get(x) for x in self.accounts_recordno if x == item['ACCOUNTID']), None)
                item.pop("ACCOUNTID", None)
            if item.get("ACCOUNTNAME") and not item.get("ACCOUNTNO"):
                item["ACCOUNTNO"] = self.accounts.get(item["ACCOUNTNAME"])
            elif not item.get("ACCOUNTNO"):
                raise Exception(    
                    f"ERROR: Account not provided or not valid for this tenant in item {item}. \n Intaccts Requires an ACCOUNTNO associated with each line item"
                )                

            self.get_items()
            if payload.get("ITEMNAME") and self.items.get(payload.get("ITEMNAME")):
                item["ITEMID"] = self.items.get(payload.get("ITEMNAME"))
                item.pop("ITEMNAME")

            self.get_departments()
            if item.get("DEPARTMENT"):
                item["DEPARTMENTID"] = self.departments[item.get("DEPARTMENT")]
                item.pop("DEPARTMENT")
            elif item.get("DEPARTMENTNAME"):
                item["DEPARTMENTID"] = self.departments[item.get("DEPARTMENTNAME")]
                item.pop("DEPARTMENTNAME")

        payload.pop("LOCATIONNAME", None)

        payload["WHENCREATED"] = payload["WHENCREATED"].split("T")[0]

        if bill:
            payload.update(bill)
            data = {"update": {"object": "accounts_payable_bills", "APBILL": payload}}
        else:
            data = {"create": {"object": "accounts_payable_bills", "APBILL": payload}}

        try:
            self.client.format_and_send_request(data)
        except Exception as e:
            # if invoice is new and attachments were posted, delete attachments
            if supdoc_id and list(data.keys())[0] == "create": 
                del_supdoc = {"delete_supdoc": {"@key": supdoc_id, "object": "supdoc"}}
                self.client.format_and_send_request(del_supdoc)
                self.logger.info(f"Supdoc '{supdoc_id}' deleted due invoice failed while being created.")
            raise Exception(e)

    def bills_upload(self, record):
        # Format data
        mapping = UnifiedMapping()
        payload = mapping.prepare_payload(record, "bills", self.target_name)

        bill = None
        if payload.get("RECORDID"):
            # validate RECORDID
            invalid_chars = r"[\"\'&<>#?]"  # characters not allowed for RECORDID [&, <, >, #, ?]
            is_id_valid = not bool(re.search(invalid_chars, payload.get("RECORDID")))

            if not is_id_valid:
                raise Exception(
                    f"RECORDID '{payload.get('RECORDID')}' contains one or more invalid characters '&,<,>,#,?'. Please provide a RECORDID that does not include these characters."
                )
            # check if record exists
            bill = self.client.get_entity(object_type="accounts_payable_bills", fields=["RECORDNO"], filter={"filter": {"equalto":{"field":"RECORDID","value": payload.get("RECORDID")}}})

        #send attachments
        supdoc_id = self.post_attachments(payload, record)
        if supdoc_id:
            payload["SUPDOCID"] = supdoc_id
        
        # Map action
        action = payload.get("STATE")
        if action:
            if action.lower() == "draft":
                payload["ACTION"] = "Draft"
        payload.pop("STATE", None)

        #include locationid at header level
        if payload.get("LOCATIONNAME"):
            self.get_locations()
            if self.locations.get(payload["LOCATIONNAME"]):
                payload["LOCATIONID"] = self.locations[payload["LOCATIONNAME"]]
                payload.pop("LOCATIONNAME")
            else:
                raise Exception(
                    f"ERROR: Location '{payload['LOCATIONNAME']}' does not exist. Did you mean any of these: {list(self.locations.keys())}?"
                )

        #look for vendorName, vendorNumber and vendorId
        if not payload.get("VENDORID"):
            self.get_vendors()
            if payload.get("VENDORNAME"):
                vendor_name = self.vendors.get(payload["VENDORNAME"])
                if vendor_name:
                    vendor_dict = {"VENDORID": self.vendors[payload["VENDORNAME"]]}
                    payload = {**vendor_dict, **payload}
                else:
                    raise Exception(
                        f"ERROR: Vendor {payload['VENDORNAME']} does not exist. Did you mean any of these: {list(self.vendors.keys())}?"
                    )

            elif payload.get("VENDORNUMBER"):
                vendor_id = payload.pop("VENDORNUMBER")
                if vendor_id in self.vendors.values():
                    payload["VENDORID"] = vendor_id
                else:
                    raise Exception(f"ERROR: VENDORID {payload['VENDORNUMBER']} not found for this account.")

        for item in payload.get("APBILLITEMS").get("APBILLITEM"):
            if payload.get("VENDORNAME"):
                self.get_vendors()
                item["VENDORID"] = self.vendors[payload["VENDORNAME"]]

            #include locationid at line level
            if payload.get("LOCATIONNAME"):
                self.get_locations()
                item["LOCATIONID"] = self.locations[payload["LOCATIONNAME"]]

            if item.get("CLASSNAME"):
                self.get_classes()
                if self.classes.get(item["CLASSNAME"]):
                    item["CLASSID"] = self.classes[item["CLASSNAME"]]
                    item.pop("CLASSNAME")
                else:
                    self.logger.info(
                        f"Skipping class due Class {payload['CLASSNAME']} does not exist. Did you mean any of these: {list(self.classes.keys())}?"
                    )


            if item.get("PROJECTNAME") and not item.get("PROJECTID"):
                self.get_projects()
                if self.projects.get(item["PROJECTNAME"]):
                    item["PROJECTID"] = self.projects[item["PROJECTNAME"]]
                else:
                    self.logger.info(
                        f"Skipping project due Project {payload['PROJECTNAME']} does not exist. Did you mean any of these: {list(self.projects.keys())}?"
                    )
            item.pop("PROJECTNAME", None)

            if payload.get("ITEMNAME") and not item.get("ITEMID"):
                self.get_items()
                item["ITEMID"] = self.items.get(payload.get("ITEMNAME"))
            item.pop("ITEMNAME", None)

            #use account instead of accountno
            self.get_accounts()
            if item.get("ACCOUNTID"):
                item["ACCOUNTNO"] = next(( self.accounts_recordno.get(x) for x in self.accounts_recordno if x == item['ACCOUNTID']), None)
                item.pop("ACCOUNTID", None)
            if item.get("ACCOUNTNAME") and not item.get("ACCOUNTNO"):
                item["ACCOUNTNO"] = self.accounts.get(item["ACCOUNTNAME"])
            if not item.get("ACCOUNTNO"):
                raise Exception(
                    f"ERROR: Account not provided or not valid for this tenant in item {item}. \n Intaccts Requires an ACCOUNTNO associated with each line item"
                )

            #departmentid is optional
            self.get_departments()
            if item.get("DEPARTMENT") and not item.get("DEPARTMENTID"):
                item["DEPARTMENTID"] = self.departments.get(item.get("DEPARTMENT"))
            item.pop("DEPARTMENT", None)
            if item.get("DEPARTMENTNAME") and not item.get("DEPARTMENTID"):
                item["DEPARTMENTID"] = self.departments.get(item.get("DEPARTMENTNAME"))
            item.pop("DEPARTMENTNAME", None)

            
            #add custom fields to the item payload
            custom_fields = item.pop("customFields", None)
            if custom_fields:
                [item.update({cf.get("name"): cf.get("value")}) for cf in custom_fields]

        payload["WHENCREATED"] = payload["WHENCREATED"].split("T")[0]

        if bill:
            payload.update(bill)
            data = {"update": {"object": "accounts_payable_bills", "APBILL": payload}}
        else:
            data = {"create": {"object": "accounts_payable_bills", "APBILL": payload}}

        try:
            self.client.format_and_send_request(data)
        except Exception as e:
            # if invoice is new and attachments were posted, delete attachments
            if supdoc_id and list(data.keys())[0] == "create": 
                del_supdoc = {"delete_supdoc": {"@key": supdoc_id, "object": "supdoc"}}
                self.client.format_and_send_request(del_supdoc)
                self.logger.info(f"Supdoc '{supdoc_id}' deleted due bill failed while being created.")
            raise Exception(e)

    def journal_entries_upload(self, record):

        # Format data
        mapping = UnifiedMapping()
        payload = mapping.prepare_payload(record, "journal_entries", self.target_name)

        if payload.get("JOURNAL"):
            payload["BATCH_TITLE"] = payload.get("JOURNAL")

        if "APBILLITEMS" in payload.keys():
            payload.pop("APBILLITEMS")

        for item in payload.get("ENTRIES").get("GLENTRY"):
            self.get_accounts()
            if item.get("ACCOUNTID"):
                item["ACCOUNTNO"] = next(( self.accounts_recordno.get(x) for x in self.accounts_recordno if x == item['ACCOUNTID']), None)
                item.pop("ACCOUNTID", None)
            if item.get("ACCOUNTNAME") and item.get("ACCOUNTNO") not in self.accounts.values():
                item["ACCOUNTNO"] = self.accounts.get(item["ACCOUNTNAME"])
                item.pop("ACCOUNTNAME")
            if not item.get("ACCOUNTNO"):
                raise Exception(
                    f"ERROR: Account not provided or not valid for this tenant on item {item}. \n Intaccts Requires an ACCOUNTNO associated with each line item"
                )
            if item.get("TR_TYPE"):
                value = 1 if item.get("TR_TYPE").lower() == "debit" else -1
                item["TR_TYPE"] = value

            self.get_departments()
            if not item.get("DEPARTMENTID"):
                if item.get("DEPARTMENT"):
                    item["DEPARTMENT"] = self.departments[item.get("DEPARTMENT")]
                    item.pop("DEPARTMENT")
                if item.get("DEPARTMENTNAME"):
                    item["DEPARTMENT"] = self.departments[item.get("DEPARTMENTNAME")]
                    item.pop("DEPARTMENTNAME")
            elif item.get("DEPARTMENTID"):
                item["DEPARTMENT"] = item.get("DEPARTMENTID")

            self.get_locations()
            if not item.get("LOCATION"):
                if item.get("LOCATIONNAME"):
                    item["LOCATION"] = self.locations[item.get("LOCATIONNAME")]
                    item.pop("LOCATIONNAME")

            if item.get("CLASSNAME"):
                self.get_classes()
                item["CLASSID"] = self.classes.get(item["CLASSNAME"])
                item.pop("CLASSNAME")

            if item.get("CUSTOMERNAME"):
                self.get_customers()
                item["CUSTOMERID"] = self.customers.get(item["CUSTOMERNAME"])
                item.pop("CUSTOMERNAME")

            if item.get("VENDORNAME"):
                self.get_vendors()
                item["VENDORID"] = self.vendors.get(item["VENDORNAME"])
                item.pop("VENDORNAME")

        payload["BATCH_DATE"] = payload["BATCH_DATE"].split("T")[0]

        data = {"create": {"object": "GLBATCH", "GLBATCH": payload}}

        self.client.format_and_send_request(data)

    def suppliers_upload(self, record):
        # Format data
        mapping = UnifiedMapping()
        payload = mapping.prepare_payload(
            record, "account_payable_vendors", self.target_name
        )
        # VENDORID is required if company does not use document sequencing
        vendor_id = payload.get("VENDORID")

        if vendor_id:
            valid_vendor_id = bool(re.match("^[A-Za-z0-9- ]*$", vendor_id))

            if valid_vendor_id:
                payload["VENDORID"] = vendor_id[
                    :20
                ]  # Intact size limit on VENDORID (20 characters)
                data = {"create": {"object": "account_payable_vendors", "VENDOR": payload}}

                self.get_vendors()
                if (not payload["VENDORID"] in self.vendors.items()) and (
                    not payload["NAME"] in self.vendors.keys()
                ):
                    self.client.format_and_send_request(data)
            else:
                self.logger.info(f"Skipping vendor with {vendor_id} due to unsupported chars. Only letters, numbers and dashes accepted")
        else:
            self.logger.info(f"Skipping vendor {payload} because vendorid is empty")

    def apadjustment_upload(self, record):
        # Format data
        mapping = UnifiedMapping()
        payload = mapping.prepare_payload(
            record, "apadjustment", self.target_name
        )

        if payload.get("action"):
            action = payload["action"]
            payload["action"] = "Draft" if action.lower() == "draft" else "Submit"

        if payload.get("vendorname"):
            self.get_vendors()
            payload["vendorid"] = self.vendors[payload["vendorname"]]
        
        if payload.get("currency"):
            payload["basecurr"] = payload["currency"]

        for item in payload.get("apadjustmentitems").get("lineitem", []):
            if item.get("accountlabel") and not item.get("glaccountno"):
                self.get_accounts()
                item["glaccountno"] = self.accounts.get(item["accountlabel"])
                item.pop("accountlabel")
            else:
                try:
                    item.pop("accountlabel")
                except:
                    pass
            
            if item.get("vendorname"):
                if not item.get("vendorid"):
                    self.get_vendors()
                    try:
                        item["vendorid"] = self.vendors[item["vendorname"]]
                    except: 
                        raise Exception(
                        f"ERROR: vendorname {item['vendorname']} not found for this account."
                    )
                item.pop("vendorname")

            if item.get("projectname"):
                if not item.get("projectid"):
                    self.get_projects()
                    try:
                        item["projectid"] = self.projects[item["projectname"]]
                    except: 
                        raise Exception(
                        f"ERROR: projectname {item['projectname']} not found for this account."
                    )
                item.pop("projectname")
            
            if item.get("locationname"):
                if not item.get("locationid"):
                    self.get_locations()
                    try:
                        item["locationid"] = self.locations[item["locationname"]]
                    except: 
                        raise Exception(
                        f"ERROR: locationname {item['locationname']} not found for this account."
                    )
                item.pop("locationname")
            
            if item.get("classname"):
                if not item.get("classid"):
                    self.get_classes()
                    try:
                        item["classid"] = self.classes[item["classname"]]
                    except: 
                        raise Exception(
                        f"ERROR: classname {item['classname']} not found for this account."
                    )
                item.pop("classname")
            
            if item.get("departmentname"):
                if not item.get("departmentid"):
                    self.get_departments()
                    try:
                        item["departmentid"] = self.departments[item["departmentname"]]
                    except: 
                        raise Exception(
                        f"ERROR: departmentname {item['departmentname']} not found for this account."
                    )
                item.pop("departmentname")
            
        # order line fields
        lines = payload.get("apadjustmentitems").get("lineitem", [])
        first_keys = ["glaccountno", "accountlabel", "amount","memo", "locationid", "departmentid", "projectid", "vendorid", "classid"]
        payload["apadjustmentitems"]["lineitem"] = [UnifiedMapping().order_dicts(line, first_keys) for line in lines]

        if payload.get("datecreated"):
            payload["datecreated"] = {
                "year": payload["datecreated"].split("-")[0],
                "month": payload["datecreated"].split("-")[1],
                "day": payload["datecreated"].split("-")[2],
            }

        ordered_payload = {}
        for key in ["vendorid", "datecreated", "adjustmentno", "action", "billno", "description", "basecurr", "currency", "exchratetype", "apadjustmentitems"]:
            if key in payload.keys():
                ordered_payload[key] = payload[key]
            elif key == "exchratetype" and key not in payload.keys():
                ordered_payload[key] = "Intacct Daily Rate"

        data = {"create_apadjustment": {"object": "apadjustment", "APADJUSTMENT": ordered_payload}}
        self.client.format_and_send_request(data, use_payload=True)

    def get_banks(self):
        # Lookup for banks
        if self.banks is None:
            banks = self.client.get_entity(
                object_type="payment_provider_bank_accounts",
                fields=["BANKACCOUNTID", "PROVIDERID"],
            )
            self.banks = banks
        return self.banks

    def query_bill(self, bill_number):
        if self.items is None:
            # Lookup for Bills
            bills = self.client.query_entity(
                object_type="accounts_payable_bills",
                bill_number=bill_number,
                fields={
                    "RECORDNO",
                    "VENDORNAME",
                    "VENDORID",
                    "RECORDID",
                    "DOCNUMBER",
                    "CURRENCY",
                    "TRX_TOTALDUE",
                },
                filters={"equalto": {"field": "RECORDNO", "value": f"{bill_number}"}},
            )
        return bills

    def pay_bill(self, record):
        if not record.get("billId"):
            raise Exception("billId is a required field")

        # Get the bill with the id
        bill = self.query_bill(record["billId"])
        if not bill:
            raise Exception(f"No bill with id={record['billId']} found.")

        # If no payment date is set, we fall back to today
        payment_date = record.get("paymentDate")

        if payment_date is None:
            payment_date = datetime.today().strftime("%m/%d/%Y")

        if not record.get("bankAccountName"):
            raise Exception("bankAccountName is a required field")

        bank_name = record["bankAccountName"]
        # TODO: not sure why we need this
        if "--" in bank_name:
            bank_name = bank_name.split("--")[0]

        if not record.get("paymentMethod"):
            raise Exception("paymentMethod is a required field")

        payload = {
            "FINANCIALENTITY": bank_name,
            "PAYMENTMETHOD": record["paymentMethod"],
            "VENDORID": record.get("vendorId") or bill["VENDORID"],
            "CURRENCY": record.get("currency") or bill["CURRENCY"],
            "PAYMENTDATE": payment_date,
            "APPYMTDETAILS": {
                "APPYMTDETAIL": {
                    "RECORDKEY": bill["RECORDNO"],
                    "TRX_PAYMENTAMOUNT": record.get("amount") or bill["TRX_TOTALDUE"],
                }
            },
        }
        data = {
            "create": {"object": "accounts_payable_payments", "APPYMT": payload}
        }
        self.client.format_and_send_request(data)

    def process_record(self, record: dict, context: dict) -> None:

        if self.stream_name == "Suppliers":
            self.suppliers_upload(record)
        if self.stream_name == "PurchaseInvoices":
            self.purchase_invoices_upload(record)
        if self.stream_name == "Bills":
            self.bills_upload(record)
        if self.stream_name == "JournalEntries":
            self.journal_entries_upload(record)
        if self.stream_name == "BillPayment":
            self.pay_bill(record)
        if self.stream_name == "APAdjustment":
            self.apadjustment_upload(record)
